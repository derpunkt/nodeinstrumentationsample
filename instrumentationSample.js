var shimmer = require('shimmer');
var fs = require('fs');
var luid = 0;
var listeners = [];
var activeListener = null;


/*
 * This is the user code
 */



// This is usually done by hooking into module._load and then calling the instrumentation modules
wrap(fs, "readFile", {
    type: "callbackLast",
    before: function(ctx) {
        console.log("Before handler called");
    },
    after: function(ctx) {
        console.log("After handler called");
    },
    enter: function(ctx) {
        console.log("Enter handler called");
    },
    exit: function(ctx) {
        console.log("Exit handler called");
    }
});
console.log("[Main] Wrapping is done");

// Let's assume we are comming from another callback like reading a file inside a request
activeListener = new ActiveListener({}, null);
doSomeTasks();


// Our application code
function doSomeTasks() {
    // Now fs should be wrapped, so let's try it out
    fs.readFile('./package.json', function(err, data) {
        if (err) throw err;
        console.log("[readFile] callback");


        fs.stat('./package.json', function(err, data) {

            if (err) throw err;
            console.log("[stat] callback");

            // And let's do it again
            fs.readFile('./package.json', function(err, data) {
                if (err) throw err;

                console.log("[readFile] callback");
            });
        });
    });
}



/*
 * This is the instrumentation
 */

// ActiveListener Object prototype
function ActiveListener(callbacks, data) {
    this.luid = ++luid;
    this.data = data === undefined ? null : data;
    listeners[this.luid] = this;
}

// Context Object prototype
function Context(original, _this, _arguments, name, metadata, syncData) {
    this.original = original;
    this._this = _this;
    this._arguments = _arguments;
    this.name = name;
    this.metadata = metadata;
    this.syncData = syncData || {};
}

// Our wrapping uitility function
function wrap(nodule, name, options) {

    console.log("[wrap] Wrapper called with " + name);

    // nodule[name] is the syntax for accessing variable members
    // it's equivalent to nodule.<someKnownMember>
    var wrapped = nodule['rx_' + name];

    console.log("[wrap] Stored original wrapper in " + 'rx_' + name);

    // Bail out as we are already wrapped
    if(wrapped) {
        console.log("[wrap] Already wrapped ... bailing out");
        return;
    }

    // Save nodule.<someKnownMember> to nodule.<rx_>someKnownMember
    nodule['rx_' + name] = nodule[name];

    // See https://github.com/othiym23/shimmer
    // syncWrap generated a wrapping function for shimmer
    // so in the end we have something like function(options) {} as 3rd argument
    shimmer.wrap(nodule, name, syncWrap(options, name));
}

// Helper that just checks if there is a listener
function asyncWrapWithListener(original, options, name, syncData) {
    // Don't wrap if there is no active transaction
    if(!activeListener) return original;

    // Note that we pass in activeListener - this means that we detach the transactional
    // context from the global one here
    return asyncWrap(original, activeListener, options, name, syncData);
}

// Wraps a callback
function modifyCallbackArgs(args, options, parentName, syncData) {

    var name = "Callback from " + (parentName || "unknown");

    switch(options.type) {

        case 'callbackLast':
            var index = args.length -1;

            // Arguments are always optional in JavaSCript and this handles the case when the user didn't pass in
            // a callback. For instance: Just delete a file and don't care for the result.
            if(typeof args[index] !== "function") {
                [].push.call(args, function(){});
            }

            // Wrap the callback
            args[index] = asyncWrapWithListener(args[index], options, name, syncData);
            break;

        case 'callbackFirst':
            if(typeof args[0] === "function") {
                // Wrap the callback
                args[0] = asyncWrapWithListener(args[0], options, name, syncData);
            }
            break;


        default:
            console.log('Applying no callback wrapping');
            return;
    }

}

// Wraps a callback
function asyncWrap(original, listener, cbs, name, syncData) {

    console.log("[asyncWrap] We are wrapping " + name);

    // The wrapped function
    return function() {

        console.log("[asyncWrap] " + name + " was called");
        console.log("[asyncWrap] Luid: " + activeListener.luid);
        var result;

        // This is the CURRENTLY active listener which can be anything from any other transaction
        // We save it because we are now entering another context
        var oldListener = activeListener;

        // Now we are on the context the callback belongs to
        activeListener = listener;

        var ctx = new Context(original, this, arguments, name, activeListener.data, syncData);

        // Call the before handler
        if(cbs.before) {
            cbs.before(ctx);
        }

        // Call the origonal function
        result = original.apply(this, arguments);

        // Call the after handler
        if(cbs.after) {
            cbs.after(ctx)
        }

        // We are done - let's restore the old listener
        activeListener = oldListener;

        return result;

    }

}

// Wraps a regular function
function syncWrap(options, name) {

    options = options || {};

    return function(original) { // Passed into shimmer and called with original function as paremeter

        console.log("[syncWrap] We are wrapping " + name);

        return function() { // This is the actual wrapped function

            console.log("[syncWrap] The wrapped " + name + " was called");
            console.log("[syncWrap] Luid: " + activeListener.luid);

            // Ignores functions that are called outside of any callback chains
            // Given the fact that a request triggers a callback all we want to see runs inside a callback chain
            // (req-res-handler creates the first activeListener and after that there is always an active listener)
            if(!activeListener) {
                console.log("No active listener - calling original function.")
                return original.apply(this, arguments);
            }

            var listenerId = activeListener.luid;
            var thisListenerId;

            var ctx = new Context(original, this, arguments, name, activeListener.data)


            // Call enter handlers
            if(options.enter) {
                options.enter(ctx);
            }

            // Prime the callback with our current context
            modifyCallbackArgs(arguments, options, name, ctx.syncData);

            // Call the original function;
            var result = original.apply(this, arguments);


            // Call enter handlers
            if(options.exit) {
                options.exit(ctx);
            }

            return result;

        }
    }
}

